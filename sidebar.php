<aside class="three columns">  
   <nav>
    <!-- pages -->
  	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('pages')) : else : ?>
    <ul class="side-nav">
    <?php $args = array(
	'depth'        => 0,
	'show_date'    => '',
	'date_format'  => get_option('date_format'),
	'child_of'     => 0,
	'exclude'      => '',
	'include'      => '',
	'title_li'     => __('<h4>Pages</h4>'),
	'echo'         => 1,
	'authors'      => '',
	'sort_column'  => 'menu_order, post_title',
	'link_before'  => '',
	'link_after'   => '',
	'walker'       => '',
	'post_type'    => 'page',
        'post_status'  => 'publish' 
    ); 
    wp_list_pages($args); ?> 
  	</ul>
	<?php endif; ?>
	<!-- posts-->
	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('posts')) : else : ?>
	<ul class="side-nav">
	<h4>Posts</h4>
    <?php
    $lastposts = get_posts('order=ASC&posts_per_page=-1');
    foreach($lastposts as $post) :
    setup_postdata($post); ?>
    <li<?php if ( $post->ID == $wp_query->post->ID ) { echo ' class="active"'; } else {} ?>>
    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
    <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  </nav>
</aside><!--sidebar-->