<?php get_header(); ?>

<div id="content" class="row">

 <section class="eight columns">

<!-- Start the Loop. -->
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
  <article <?php post_class(); ?>>
  
 <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

 <time pubdate="<?php the_time('m-d-Y H:i:s T'); ?>">
 	<i class="icon-calendar"></i> <span class="secondary radius label"><?php the_time('F j, Y'); ?></span>
 </time>
 <p class="content"> <?php the_content(); ?></p>
   
		<!-- categories -->
		<p>
		 <span>
		 <i class="icon-bookmark"></i> <span class="secondary radius label"><?php the_category(', '); ?></span>
		 </span>&nbsp;&nbsp;&nbsp;
		<!--  tags -->
		 <span>
		 <?php the_tags('<i class="icon-tag"></i> <span class="secondary radius label">','</span> <span class="secondary radius label">','</span>'); ?>
		 </span>
		</p>

   	<ul class="button-group radius">
   		<li><a href="http://twitter.com/home?status=<?php the_title(); ?>%20-%20<?php bloginfo('name'); ?>%20<?php the_permalink() ?>" class="secondary button radius"><i class="icon-twitter"></i></a></li>
   		<li><a href="http://www.facebook.com/share.php?u=<?php the_permalink() ?>" class="secondary button radius"><i class="icon-facebook"></i></a></li>
   		<li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" class="secondary button radius"><i class="icon-google-plus"></i></a></li>
   </ul>
 
 <?php comments_template(); ?> 
 <hr />
 </article> <!-- post -->
 
 <?php endwhile; else: ?>

 <p>Oops. No posts! WTF?</p>

 <?php endif; ?>
 
 <nav>
 	<?php posts_nav_link(' &#8212; ', __('<span class="button">&laquo; Prev</class>'), __('<span class="button">Next &raquo;</span>')); ?>
 </nav>
 
 </section>
 
<?php get_sidebar(); ?>
 
</div><!--content-->
<?php get_footer(); ?>

<!-- Remember- Comics are best enjoyed with friends. -->
