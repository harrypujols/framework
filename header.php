<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="<?php browser_detect(); echo ' no-js'; ?>" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <!-- feed -->
	<link href="<?php bloginfo('rss2_url'); ?>" title="RSS - <?php bloginfo('name'); ?>" type="application/rss+xml" rel="alternate">
	<link href="<?php bloginfo('atom_url'); ?>" title="Atom - <?php bloginfo('name'); ?>" type="application/atom+xml" rel="alternate">
  <!-- icons -->
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.ico" />
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />
  <title>
	<?php
	wp_title( '|', true, 'right' );
	// Add the site's name.
	bloginfo( 'name' );
	// Add the site's description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?>
  </title>
  
<?php wp_head(); ?>

  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>
<body <?php body_class(); ?>>
	<div id="container">
	<?php get_template_part( 'header', 'bar' ); ?>
<header class="row">
	<?php get_template_part( 'header', 'image' ); ?>
</header>

<!-- header menu -->
<nav class="row">
<?php 
if ( has_nav_menu( 'header-menu' ) ) {
	wp_nav_menu( array( 
	'theme_location'  => 'header-menu',
	'container'       => 'div', 
	'container_class' => 'twelve columns', 
	'menu_class'      => 'menu',
	'menu_id'         => 'header-nav', 
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'items_wrap'      => '<ul id="%1$s" class="nav-bar">%3$s</ul>',
	'depth'           => 0,
	)); 
}
?>
</nav>