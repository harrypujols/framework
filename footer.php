   <footer class="row">
   <hr />
    <div class="twelve columns">
      <p>Prepared by <a href="http://harrypujols.com">Harry Pujols</a> using basically &nbsp;<i class="icon-html5"></i>&nbsp;&nbsp;<i class="icon-css3"></i>&nbsp;&nbsp;<i class="icon-jquery"></i>&nbsp;&nbsp;<i class="icon-wordpress"></i></p>
    </div><!-- twelve columns -->
  </footer>
  </div><!-- container -->
<?php wp_footer(); ?>
</body>
		<!-- remove title attribute from images -->
		<script>
		var links = document.getElementsByTagName('img');
		for (var i = 0; i < links.length; i++) {
		  links[i].removeAttribute('title');
		}
		</script>
</html>