    <!-- if the header image is selected -->
    <?php $header_image = get_header_image(); ?>
    <?php if ( $header_image) : ?>
    <div id="main-header" class="twelve columns with-image">
    <!-- title -->
     		<hgroup id="header-title">
				<h1 id="site-title"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
			</hgroup>
	<!-- image-->
     	<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo('name'); ?>" />
     	
    <!-- if there's no header image -->
    <?php else: ?>
    <div id="main-header" class="twelve columns">
    <!-- title -->
     		<hgroup id="header-title">
				<h1 id="site-title"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
			</hgroup>
    
    <?php endif; ?>
    </div><!-- twelve columns -->