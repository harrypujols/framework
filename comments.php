
<?php if ( !empty($post->post_password) && $_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : ?>
<p><i class="icon-lock"></i> Password</p>
<?php return; endif; ?>

<?php if ( $comments ) : ?>


<div class="commentheader"><h5>Comments</h5></div>

<?php foreach ($comments as $comment) : ?>

<div class="comment panel">
<div class="gravatarside"><?php if (function_exists('get_avatar')) { echo get_avatar($comment,$size='48'); } ?></div>
<div class="commenticon">
<strong>
<?php if ('' != get_comment_author_url()) { ?><a href="<?php comment_author_url(); ?>"><?php comment_author() ?></a><?php } else { comment_author(); } ?>
</strong>
<p><time> <?php comment_date() ?> at <?php comment_time(); ?></time> <?php edit_comment_link('<i class="icon-edit"></i>','&nbsp;'); ?> </p>
</div>
<p>
<blockquote class="commenttext"><?php comment_text() ?></blockquote>
</p>
<hr />
<?php if ($comment->comment_approved == '0') : ?>
<p>Thank you for your comment! It has been added to the moderation queue and will be published here if approved by the webmaster.</p>
<?php endif; ?>
</div>



<?php endforeach; ?>
<?php endif; ?>

<?php if ($post->comment_status == "open") : ?>
<div id="respond" class="callout panel">
<div class="navigation"><h5>Say something</h5></div>
<?php if (get_option('comment_registration') && !$user_ID) : ?>
<p><i class="icon-login icon-white"></i> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">Login</a></p>
</div>

<?php else : ?>
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
<?php if ($user_ID) : ?>
<div class="loggedin">
<p>
<i class="icon-user icon-white"></i> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>
<br />
<a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out"><i class="icon-logout icon-white"></i> Log out</a>
</p>
</div>

<?php else : ?>

	<div class="row collapse">
		<div class="mobile-one one columns">
	<label class="prefix" for="author"><i class="icon-user icon-white"></i> </label>
		</div>
		<div class="mobile-three ten columns end">
	<input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" tabindex="1" / placeholder="Name" /><br/><br/>
		</div>
	</div>
	<div class="row collapse">
		<div class="mobile-one one columns">
	<label class="prefix" for="email"><i class="icon-envelope icon-white"></i> </label>
		</div>
		<div class="mobile-three ten columns end">
	<input type="email" name="email" id="email" value="<?php echo $comment_author_email; ?>" tabindex="2" placeholder="E-mail" /><br/><br/>
		</div>
	</div>
	<div class="row collapse">
		<div class="mobile-one one columns">
	<label class="prefix" for="url"><i class="icon-globe icon-white"></i> </label>
		</div>
		<div class="mobile-three ten columns end">
	<input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" tabindex="3" placeholder="Website" /><br/><br/>
		</div>
	</div>


<?php endif; ?>
<textarea name="comment" id="comment" cols="45" rows="4" tabindex="4" placeholder="Your comment"></textarea><br/><br/>
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
<input type="submit" name="submit" value="Send" class="radius button" tabindex="5" />
<?php do_action('comment_form', $post->ID); ?>
</form>
</div>

<?php endif; ?>
<?php endif; ?>

