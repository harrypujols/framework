<?php

//add Foundation js
function responsive_scripts_basic()
{
	//register scripts for our theme
	wp_register_script('foundation-mod', get_template_directory_uri() . '/js/modernizr.foundation.js', array( 'jquery' ), true );
	wp_register_script('foundation-main', get_template_directory_uri() . '/js/foundation.js', true );
	wp_register_script('foundation-app', get_template_directory_uri() . '/js/scripts.js', true );
	wp_enqueue_script( 'foundation-mod' );
	wp_enqueue_script( 'foundation-main' );
	wp_enqueue_script( 'foundation-app' );
}
add_action( 'wp_enqueue_scripts', 'responsive_scripts_basic', 5 );

//add Foundation styles
function responsive_styles()
{
	//register styles for our theme
	wp_register_style( 'foundation-style', get_template_directory_uri() . '/css/foundation.css', array(), 'all' );
	wp_register_style( 'foundation-appstyle', get_template_directory_uri() . '/style.css', array(), 'all');
	wp_enqueue_style( 'foundation-style' );
	wp_enqueue_style( 'foundation-appstyle' );
}
add_action( 'wp_enqueue_scripts', 'responsive_styles' );

//register sidebar widgets
  
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name'          => 'pages',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h4 class="widgettitle">',
	'after_title'   => '</h4>'
));
register_sidebar(array(
	'name'=>'posts',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h4 class="widgettitle">',
	'after_title'   => '</h4>'
));


//custom header support

$defaults = array(
	'default-image'          => get_template_directory_uri() . '/img/placeholder_960x315.png',
	'random-default'         => false,
	'width'                  => 960,
	'height'                 => 315,
	'flex-height'            => true,
	'flex-width'             => true,
	'default-text-color'     => '3fa8f2',
	'header-text'            => true,
	'uploads'                => true,
	// Callback for styling the header
	'wp-head-callback'       => 'fw_header_style',
);

add_theme_support( 'custom-header', $defaults );

// Load custom header options.
require_once( get_template_directory() . '/custom-header.php' );

//custom background support
$defaults = array(
	'wp-head-callback'       => '_custom_background_cb'
);
add_theme_support( 'custom-background', $defaults );

//custom post formats support
$defaults = array(
	'image', 
	'aside',
	'gallery', 
	'status', 
	'quote', 
	'video',
	'link'
);
add_theme_support( 'post-formats', $defaults);
	
//register custom menus
function register_menus() {
	register_nav_menus( array(
		'header-menu' => __( 'Header Menu' ),
	));
}
add_action( 'init', 'register_menus' );

//custom search form
function custom_search( $form ) {

    $form = '
    
    <form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div class="row collapse">
    <div class="ten mobile-three columns">
    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search" />
    </div>
    <div class="two mobile-one columns">
    <button class="postfix secondary button" type="submit" id="searchsubmit"><i class="icon-search"></i></button>
    </div>
    </div>
    </form>
    ';

    return $form;
}
add_filter( 'get_search_form', 'custom_search' );


// Load up the theme options page and related code.
require_once( get_template_directory() . '/theme-options.php' );

//Add a conditional browser class
function browser_detect(){
		$ipod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iphone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$firefox = stripos($_SERVER['HTTP_USER_AGENT'],"Firefox");
		$safari = stripos($_SERVER['HTTP_USER_AGENT'],"Safari");
		$ie = stripos($_SERVER['HTTP_USER_AGENT'],"MSIE");
		$opera = stripos($_SERVER['HTTP_USER_AGENT'],"Opera");
		$chrome = stripos($_SERVER['HTTP_USER_AGENT'],"Chrome");
		//detecting device 
		if ($ipod == true || $iphone == true){
					echo "iphone";
		}
		elseif($firefox == true){
					echo "firefox";
		}
		elseif($ie == true){
					echo "explorer";
		}
		elseif($chrome == true){
					echo "chrome";
		}
		elseif($safari == true){
					echo "safari";
		}
		elseif($opera == true){
					echo "opera";
		}
		else{
			echo "unknown";
		}
	};

//end
?>