<?php

if ( ! function_exists( 'fw_header_style' ) ) :

	function fw_header_style() {

	
	// CUSTOM THEME COLOUR
	global $fw_options;
	$fw_settings = get_option( 'fw_options', $fw_options );
	$theme_colour = $fw_settings['theme_colour'];
	
	// CUSTOM TEXT COLOUR
	$text_color = get_header_textcolor();

	// If no custom options for text are set, let's bail.
	if ( $text_color == get_theme_support( 'custom-header', 'default-text-color' ) )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == $text_color ) :
	?>
		#site-title,
		#site-description {
			display: none;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description,
		#access a,
		#access li.current_page_item > a, 
		#access li.current_page_parent > a, 
		#access li.current-page-ancestor > a, 
		#access li.current-post-ancestor > a,
		#container a  {
			color: #<?php echo $text_color; ?> !important;
		}
		
	  <?php endif; ?>
	</style>
	<?php
	
}


endif; // fw_header_style

?>