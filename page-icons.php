<?php 
/*
Template Name: icons
*/	
?>

<?php get_header(); ?>

 <header class="row">
    <div class="twelve columns">
      <h3>The Icons</h3>
      <p>Using i tag with the icon class. For white icons, add icon-white class (you know, like in Twitter Bootstrap).</p>
    </div>
  </header>

      <!-- Grid  -->
      <section class="row" id="icons">
        <ul class="three columns">
        	<li><i class="icon-bookmark"></i> icon-bookmark</li>
        	<li><i class="icon-link"></i> icon-link</li>
        	<li><i class="icon-edit"></i> icon-edit</li>
        	<li><i class="icon-envelope"></i> icon-envelope</li>
        	<li><i class="icon-camera"></i> icon-camera</li>
        	<li><i class="icon-html5"></i> icon-html5</li>
        	<li><i class="icon-badge"></i> icon-badge</li>
        	<li><i class="icon-globe"></i> icon-globe</li>
        	<li><i class="icon-calendar"></i> icon-calendar</li>
        	<li><i class="icon-heart"></i> icon-heart</li>
        	<li><i class="icon-ul"></i> icon-ul</li>
        </ul>
        <ul class="three columns">
        	<li><i class="icon-map-marker"></i> icon-map-marker</li>
        	<li><i class="icon-user"></i> icon-user</li>
        	<li><i class="icon-comment"></i> icon-comment</li>
        	<li><i class="icon-chat"></i> icon-chat</li>
        	<li><i class="icon-clock"></i> icon-clock</li>
        	<li><i class="icon-time"></i> icon-time</li>
        	<li><i class="icon-external"></i> icon-external</li>
        	<li><i class="icon-logout"></i> icon-logout</li>
        	<li><i class="icon-login"></i> icon-login</li>
        	<li><i class="icon-arrow-right"></i> icon-arrow-right</li>
        	<li><i class="icon-arrow-left"></i> icon-arrow-left</li>
        </ul>
         <ul class="three columns">
        	<li><i class="icon-share"></i> icon-share</li>
        	<li><i class="icon-tumblr"></i> icon-tumblr</li>
        	<li><i class="icon-twitter"></i> icon-twitter</li>
        	<li><i class="icon-facebook"></i> icon-facebook</li>
        	<li><i class="icon-google-plus"></i> icon-google-plus</li>
        	<li><i class="icon-linkedin"></i> icon-linkedin</li>
        	<li><i class="icon-vimeo"></i> icon-vimeo</li>
        	<li><i class="icon-pinterest"></i> icon-pinterest</li>
        	<li><i class="icon-blogger"></i> icon-blogger</li>
        	<li><i class="icon-wordpress"></i> icon-wordpress</li>
        	<li><i class="icon-github"></i> icon-github</li>
        </ul>
         <ul class="three columns">
        	<li><i class="icon-instagram"></i> icon-instagram</li>
        	<li><i class="icon-flickr"></i> icon-flickr</li>
        	<li><i class="icon-behance"></i> icon-behance</li>
        	<li><i class="icon-home"></i> icon-home</li>
        	<li><i class="icon-rss"></i> icon-rss</li>
        	<li><i class="icon-chevron-left"></i> icon-chevron-left</li>
        	<li><i class="icon-chevron-right"></i> icon-chevron-right</li>
        	<li><i class="icon-info"></i> icon-info</li>
        	<li><i class="icon-question-sign"></i> icon-question-sign</li>
        	<li><i class="icon-forward"></i> icon-forward</li>
        	<li><i class="icon-fullscreen"></i> icon-fullscreen</li>
        </ul>
        <ul class="three columns">
        	<li><i class="icon-image"></i> icon-image</li>
        	<li><i class="icon-movie"></i> icon-movie</li>
        	<li><i class="icon-music"></i> icon-music</li>
        	<li><i class="icon-mark-left"></i> icon-mark-left</li>
        	<li><i class="icon-mark-right"></i> icon-mark-right</li>
        	<li><i class="icon-next"></i> icon-next</li>
        	<li><i class="icon-prev"></i> icon-prev</li>
        	<li><i class="icon-x-mark"></i> icon-x-mark</li>
        	<li><i class="icon-minus-sign-reverse"></i> icon-minus-sign-reverse</li>
        	<li><i class="icon-minus-sign"></i> icon-minus-sign</li>
        	<li><i class="icon-sync"></i> icon-sync</li>
        </ul>
         <ul class="three columns">
        	<li><i class="icon-skype"></i> icon-skype</li>
        	<li><i class="icon-tablet"></i> icon-tablet</li>
        	<li><i class="icon-search"></i> icon-search</li>
        	<li><i class="icon-return"></i> icon-return</li>
        	<li><i class="icon-share-alt"></i> icon-share-alt</li>
        	<li><i class="icon-star-empty"></i> icon-star-empty</li>
        	<li><i class="icon-star"></i> icon-star</li>
        	<li><i class="icon-shuffle"></i> icon-shuffle</li>
        	<li><i class="icon-download"></i> icon-download</li>
        	<li><i class="icon-upload"></i> icon-upload</li>
        	<li><i class="icon-cloud"></i> icon-cloud</li>
        </ul>
         <ul class="three columns">
        	<li><i class="icon-cog"></i> icon-cog</li>
        	<li><i class="icon-lizard"></i> icon-lizard</li>
        	<li><i class="icon-gear"></i> icon-gear</li>
        	<li><i class="icon-back"></i> icon-back</li>
        	<li><i class="icon-play"></i> icon-play</li>
        	<li><i class="icon-computer"></i> icon-computer</li>
        	<li><i class="icon-error"></i> icon-error</li>
        	<li><i class="icon-exclamation-sign"></i> icon-exclamation-sign</li>
        	<li><i class="icon-expand"></i> icon-expand</li>
        	<li><i class="icon-eye"></i> icon-eye</li>
        	<li><i class="icon-rewind"></i> icon-rewind</li>
        </ul>
         <ul class="three columns">
        	<li><i class="icon-fast-forward"></i> icon-fast-forward</li>
        	<li><i class="icon-laptop"></i> icon-laptop</li>
        	<li><i class="icon-smartphone"></i> icon-smartphone</li>
        	<li><i class="icon-phone"></i> icon-phone</li>
        	<li><i class="icon-file"></i> icon-file</li>
        	<li><i class="icon-folder"></i> icon-folder</li>
        	<li><i class="icon-backwards"></i> icon-backwards</li>
        	<li><i class="icon-like"></i> icon-like</li>
        	<li><i class="icon-warning"></i> icon-warning</li>
        	<li><i class="icon-ok"></i> icon-ok</li>
        	<li><i class="icon-list"></i> icon-list</li>
        </ul>
         <ul class="three columns end">
        	<li><i class="icon-lock"></i> icon-lock</li>
        	<li><i class="icon-flag"></i> icon-flag</li>
        	<li><i class="icon-dislike"></i> icon-dislike</li>
        	<li><i class="icon-sound"></i> icon-sound</li>
        	<li><i class="icon-tag"></i> icon-tag</li>
        	<li><i class="icon-hand"></i> icon-hand</li>
        	<li><i class="icon-sign"></i> icon-sign</li>
        	<li><i class="icon-save"></i> icon-save</li>
        	<li><i class="icon-arrow-up"></i> icon-arrow-up</li>
        	<li><i class="icon-arrow-down"></i> icon-arrow-down</li>
        	<li><i class="icon-jquery"></i> icon-jquery</li>
        	<li><i class="icon-css3"></i> icon-css3</li>
        </ul>
      </section>
      <header class="row">
    <div class="twelve columns">
      <h3>Examples</h3>
    </div>
  </header>
  <section class="row">
  <!--  buttons -->
   <div class="four columns">
   <h4>In buttons</h4>
   <p><a href="#" class="small radius button"><i class="icon-like icon-white"></i> Like this shit</a></p>
   <p><a href="#" class="small success round button">Download <i class="icon-download icon-white"></i></a></p>
   <p>
   	<ul class="button-group radius">
   		<li><a href="#" class="secondary button radius"><i class="icon-backwards"></i></a></li>
   		<li><a href="#" class="secondary button radius"><i class="icon-play"></i></a></li>
   		<li><a href="#" class="secondary button radius"><i class="icon-forward"></i></a></li>
   </ul>
   </p>
   </div>
   
   <div class="four columns">
   
   <!-- forms -->
   <h4>In forms</h4>
    <label>Username</label>
   <div class="row collapse">
   <div class="two mobile-one columns">
        <span class="prefix"><i class="icon-user"></i></span>
      </div><!-- two mobile-one columns -->
      <div class="ten mobile-three columns">
        <input type="text" placeholder="Your name here" />
      </div><!-- ten mobile-three columns -->
   </div><!-- row collapse -->
   
   
   <div class="row collapse">
      <div class="ten mobile-three columns">
        <input type="text" placeholder="Search" />
      </div><!-- ten mobile-three columns -->
      <div class="two mobile-one columns">
      <span class="postfix"><a href="#"><i class="icon-search"></i></a></span>
       </div><!-- two mobile-one columns -->
   </div><!-- row collapse -->
      
   </div><!-- four columns -->
   
     <!--  pannels -->
   <div class="four columns">
   <h4>In panels</h4>
   <div class="alert alert-box"><i class="icon-warning icon-white"></i> You fucked up!<a href="" class="close">&times;</a></div>
   <div class="panel radius">
	   <h5>This is a panel.</h5>
	   <p><i class="icon-html5"></i> <i class="icon-css3"></i> <i class="icon-jquery"></i> <i class="icon-wordpress"></i></p>
	   <p>Made with HTML5, CSS3 and jQuery</p>
   </div>
   
   </div>
   
  </section>
  
   <section class="row">
   <hr />
    <div class="twelve columns">
      <p><i class="icon-exclamation-sign"></i> <b>Note:</b> The lion's share of these icons were picked from <a href="http://iconmonstr.com">Iconmonstr</a>. I'm not taking all the credit.</p>
    </div>
  </section>
<?php get_footer(); ?>
