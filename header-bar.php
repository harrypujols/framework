<!-- icons menu -->
<nav id="top-bar" class="top-bar">
	<ul>
			   <li><a href="<?php bloginfo('url'); ?>"><i class="icon-home icon-white"></i></a></li>
			   <li><a href="<?php bloginfo('rss2_url'); ?>"><i class="icon-rss icon-white"></i></a></li>
			   <?php
                	global $fw_options;
					$fw_settings = get_option( 'fw_options', $fw_options );
				?>
                
                <?php if( $fw_settings['twitter'] != '' ) : ?>
				<li><a href="https://twitter.com/<?php echo $fw_settings['twitter']; ?>"><i class="icon-twitter icon-white"></i></a></li>
				<?php endif; ?>
				<?php if( $fw_settings['facebook'] != '' ) : ?>
				<li><a href="https://facebook.com/<?php echo $fw_settings['facebook']; ?>"><i class="icon-facebook icon-white"></i></a></li>
				<?php endif; ?>
	</ul>
</nav><!-- top-bar -->
<!-- icons menu -->