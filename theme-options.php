<?php

// Default options values
$fw_options = array(
	'twitter' => '',
	'facebook' => '',
);

if ( is_admin() ) : // Load only if we are viewing an admin page

function fw_register_settings() {
	// Register settings and call sanitation functions
	register_setting( 'fw_theme_options', 'fw_options', 'fw_validate_options' );
}

add_action( 'admin_init', 'fw_register_settings' );

function fw_theme_options() {
	// Add theme options page to the addmin menu
	add_theme_page( 'Theme Options', 'Theme Options', 'edit_theme_options', 'theme_options', 'fw_theme_options_page' );
}

add_action( 'admin_menu', 'fw_theme_options' );

// Function to generate options page
function fw_theme_options_page() {
	global $fw_options, $fw_categories, $fw_layouts;

	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false; // This checks whether the form has just been submitted. ?>

	<div class="wrap">

	<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options' ) . "</h2>";
	// This shows the page's name and an icon if one has been provided ?>

	<?php if ( false !== $_REQUEST['updated'] ) : ?>
	<div class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
	<?php endif; // If the form has just been submitted, this shows the notification ?>

	<form method="post" action="options.php">

	<?php $settings = get_option( 'fw_options', $fw_options ); ?>
	
	<?php settings_fields( 'fw_theme_options' );
	/* This function outputs some hidden fields required by the form,
	including a nonce, a unique number used to ensure the form has been submitted from the admin page
	and not somewhere else, very important for security */ ?>

	<table class="form-table"><!-- Grab a hot cup of coffee, yes we're using tables! -->

    <!-- twitter handle -->
	<tr valign="top"><th scope="row"><label for="twitter">Twitter Handle</label></th>
	<td>
	<input id="twitter" name="fw_options[twitter]" type="text" value="<?php  esc_attr_e($settings['twitter']); ?>" />
	</td>
	</tr>
	<!-- facebook handle -->
	<tr valign="top"><th scope="row"><label for="facebook">Facebook Page Name</label></th>
	<td>
	<input id="facebook" name="fw_options[facebook]" type="text" value="<?php  esc_attr_e($settings['facebook']); ?>" />
	</td>
	</tr>
	
	</table>
	
	
	<p class="submit"><input type="submit" class="button-primary" value="Save Options" /></p>
	</form>
	</div><!-- wrap -->
	<?php
}

function fw_validate_options( $input ) {
	global $fw_options, $fw_categories, $fw_layouts;

	$settings = get_option( 'fw_options', $fw_options );
	
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['twitter'] = wp_filter_nohtml_kses( $input['twitter'] );
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['facebook'] = wp_filter_nohtml_kses( $input['facebook'] );
		
	return $input;
}

endif;  // EndIf is_admin()
?>
