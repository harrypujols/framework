<?php get_header(); ?>

<div id="content" class="row">

 <section class="eight columns">

<!-- Start the Loop. -->
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
  <article <?php post_class(); ?>>
  
 <h3 class="content"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
 <time pubdate="<?php the_time('m-d-Y H:i:s T'); ?>">
 	<i class="icon-calendar"></i> <span class="secondary radius label"><?php the_time('F j, Y'); ?></span>
 </time>
 <p class="content"> <?php the_content(); ?></p>
   

		<!-- categories -->
		<p>
		 <span>
		 <i class="icon-bookmark"></i> <span class="secondary radius label"><?php the_category(', '); ?></span>
		 </span>&nbsp;&nbsp;&nbsp;
		<!--  tags -->
		 <span>
		 <?php the_tags('<i class="icon-tag"></i> <span class="secondary radius label">','</span> <span class="secondary radius label">','</span>'); ?>
		 </span>
		</p>
		<!-- comments -->
		<p>
		 <a href="<?php comments_link(); ?>"><i class="icon-comment"></i> <?php comments_number('<span class="radius label">comments</span>','<span class="success radius label">1 comment</span>','<span class="success radius label">% comments</span>'); ?></a>
		</p>

 <hr />
 </article> <!-- post -->
 
 <?php endwhile; else: ?>
 	<div class="alert alert-box">
	 	<p><i class="icon-warning icon-white"></i> Sorry. There's no content here.<a href="" class="close">&times;</a></p> 
    </div><!-- alert box -->   
    <div class="row">
	 		<div class="six columns">
	 	<?php get_search_form( 'custom_search' ); ?>
	 		</div><!-- four columns -->
	 </div><!-- row -->
 <?php endif; ?>
 
 <nav>
 	<?php posts_nav_link(' &#8212; ', __('<span class="button radius"><i class=" icon-backwards icon-white"></i></class>'), __('<span class="button radius"><i class=" icon-forward icon-white"></i></span>')); ?>
 </nav>
 
 </section>
 
<?php get_sidebar(); ?>
 
</div><!--content-->
<?php get_footer(); ?>

<!-- Remember- Comics are best enjoyed with friends. -->
